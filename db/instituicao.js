var mysql = require('mysql');
var conexao = require('./conexao.json');
var schema = require('../schema');
var Validator = require('jsonschema').Validator;
var v = new Validator();
var pool  = mysql.createPool(conexao);

exports.findInstituicao = function (req, res) {
  pool.getConnection(function(err, connection) {
    if (err) throw err;
    connection.query( 'SELECT * FROM `instituicao` WHERE `id` = ?',[req.params.id], function(err, rows) {
      if (err) {
        console.log(err);
        res.send(err);
      }
      else {
        res.send(rows);
        connection.release();
      }
    });
  });
}

exports.findAllInstituicao = function (req, res) {
  pool.getConnection(function(err, connection) {
	if (err) throw err;
    connection.query( 'SELECT * FROM `instituicao`', function(err, rows) {
      if (err) {
        console.log(err);
        res.send(err);
      }
      else {
        res.send(rows);
        connection.release();
      }
    });
  });
}

exports.newInstituicao = function (req, res) {
	pool.getConnection(function(err, connection) {
		var resultado = v.validate(req.body, schema.instituicao);
		if (resultado.errors.length){
			res.json({"Error" : true, "Message" : resultado.errors});
		} else {
			var query = "INSERT INTO `instituicao` VALUES (0,?,?)";
			var table = [req.body.nome, req.body.sigla];
			query = mysql.format(query,table);
			connection.query(query, function(err, rows) {
				if (err) {
					console.log(err);
					res.json({"Error" : true, "Message" : "Erro ao executar a operação. Contate o administrador."});
				}
				else {
					res.json({"Error" : false, "Message" : "Instituição adicionada com sucesso."});
					connection.release();
				}
			});
		}
	});
}

exports.updateInstituicao = function (req, res) {
	pool.getConnection(function(err, connection) {
		var resultado = v.validate(req.body, schema.instituicao);
		if (resultado.errors.length){
			res.json({"Error" : true, "Message" : resultado.errors});
		} else {
			var query = "UPDATE `instituicao` SET nome = ?, sigla = ? WHERE id = ?";
			var table = [req.body.nome, req.body.sigla, req.params.id];
			query = mysql.format(query,table);
			connection.query(query, function(err, rows) {
				if (err) {
					res.json({"Error" : true, "Message" : "Erro ao executar a operação. Contate o administrador."});
				}
				else {
					res.json({"Error" : false, "Message" : "Instituição atualizada com sucesso."});
					connection.release();
				}
			});
		}
	});
}

exports.deleteInstituicao = function(req, res){
	pool.getConnection(function(err, connection) {
		if (err){
			console.log(err);
			res.json({"Error" : true, "Message" : err});
		}
		else {
			connection.query("DELETE FROM `instituicao` WHERE `id` = ?",[req.params.id], function(err, rows){
				if (err) {
						console.log(err);
						res.json({"Error" : true, "Message" : "Erro ao executar a operação. Verifique se há questões ligadas a instituição."});
				}
        else res.json({"Error" : false, "Message" : "Questão removida com sucesso."});
			});
			connection.release();
		}
	});
}
