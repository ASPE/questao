var mysql = require('mysql');
var conexao = require('./conexao.json');
var schema = require('../schema');
var Validator = require('jsonschema').Validator;
var v = new Validator();
var pool  = mysql.createPool(conexao);

//Para Teste
var usuario = require('../store/usuario.json');

exports.loginUsuario = function(req, res){
		if (req.body.matricula == usuario["matricula"] && req.body.senha == usuario["senha"])
      res.json({"Error" : false, "Message" : "Logado."})
    else
      res.json({"Error" : true, "Message" : "Matrícula ou Senha inválidos."});
}
