var mysql = require('mysql');
var conexao = require('./conexao.json');
var schema = require('../schema');
var Validator = require('jsonschema').Validator;
var v = new Validator();
var pool  = mysql.createPool(conexao);

avaliarQuestao = function(id, status, res){
	pool.getConnection(function(err, connection) {
		if (err){
			console.log(err);
			res.json({"Error" : true, "Message" : err});
		}
		else {
			connection.query("UPDATE `questao` SET `id_status` = ? WHERE id = ?", [status, id], function(err, rows){
				if (err) {
						console.log(err);
						res.json({"Error" : true, "Message" : "Erro ao executar a operação. Contate o Administrador."});
				}
        else {
			    connection.release();
					res.json({"Error" : false, "Message" : "Status alterado com sucesso."});
				}
			});
		}
	});
}

exports.findStatus = function(req, res){
  pool.getConnection(function(err, connection) {
		if (err){
			console.log(err);
			res.json({"Error" : true, "Message" : err});
		}
		else {
			connection.query("SELECT * FROM `status` WHERE id = ?", [req.params.id], function(err, rows){
				if (err) {
					console.log(err);
					res.json({"Error" : true, "Message" : "Erro ao executar a operação. Contate o administrador"});
				}
        else {
          connection.release();
          res.json(rows);
        }
			});
		}
	});
}
exports.findOpcoes = function(req, res){
  pool.getConnection(function(err, connection) {
		if (err){
			console.log(err);
			res.json({"Error" : true, "Message" : err});
		}
		else {
			connection.query("SELECT * FROM `opcao` WHERE id_questao = ?", [req.params.id], function(err, rows){
				if (err) {
					console.log(err);
					res.json({"Error" : true, "Message" : "Erro ao executar a operação. Contate o administrador"});
				}
        else {
          connection.release();
          res.json(rows);
        }
			});
		}
	});
}

exports.findQuestao = function (req, res) {
  pool.getConnection(function(err, connection) {
    if (err) throw err;
    connection.query( 'SELECT * FROM `questao` WHERE `id` = ?',[req.params.id], function(err, rows) {
      if (err) {
        console.log(err);
        res.send(err);
      }
      else {
        res.send(rows);
        connection.release();
      }
    });
  });
}

exports.findQuestoesPorStatus = function (req, res) {
  pool.getConnection(function(err, connection) {
    if (err) throw err;
    connection.query( 'SELECT * FROM `questao` WHERE `id_status` = ?',[req.params.id], function(err, rows) {
      if (err) {
        console.log(err);
        res.send(err);
      }
      else {
        res.send(rows);
        connection.release();
      }
    });
  });
}

exports.findAllQuestao = function (req, res) {
  pool.getConnection(function(err, connection) {
	if (err) throw err;
    connection.query( 'SELECT * FROM `questao`', function(err, rows) {
      if (err) {
        console.log(err);
        res.send(err);
      }
      else {
        res.send(rows);
        connection.release();
      }
    });
  });
}

exports.newQuestao = function (req, res) {
	pool.getConnection(function(err, connection) {
		v.addSchema(schema.opcao, '/SimpleOpcao');
		var resultado = v.validate(req.body, schema.questao);
		if (resultado.errors.length){
			res.json({"Error" : true, "Message" : resultado.errors});
		} else {
			var query = "INSERT INTO `questao` VALUES (0,?,?,?,?,?,1)";

			var table = [req.body.enunciado,
						 req.body.ano,
						 req.body.transversal,
             req.body.resposta,
						 req.body.id_instituicao];
			query = mysql.format(query,table);
			connection.query(query, function(err, rows) {
				if (err) {
					console.log(err);
					res.json({"Error" : true, "Message" : "Erro ao executar a operação. Contate o administrador."});
				}
				else {
					for (var i=0;i<req.body.opcoes.length;i++){
						var query = "INSERT INTO `opcao` VALUES (0,?,?)";
						var table = [req.body.opcoes[i].nome, rows.insertId];
						query = mysql.format(query,table);
						connection.query(query, function(err, rowsOpcao) {
							if (err) {
								console.log(err);
								res.json({"Error" : true, "Message" : "Erro ao executar a operação. Contate o administrador."});
							}
						});
					}
					res.json({"Error" : false, "Message" : "Questão adicionada com sucesso."});
					connection.release();
				}
			});
		}
	});
}

exports.updateQuestao = function (req, res) {
	pool.getConnection(function(err, connection) {
		v.addSchema(schema.opcao, '/SimpleOpcao');
		var resultado = v.validate(req.body, schema.questao);
		if (resultado.errors.length){
			res.json({"Error" : true, "Message" : resultado.errors});
		} else {
			var query = "UPDATE `questao` SET `enunciado` = ?, `ano` = ?, `transversal` = ?, `id_instituicao` = ?, `resposta` = ?, `id_status` = 1 WHERE id = ?";
			var table = [req.body.enunciado,
      						 req.body.ano,
      						 req.body.transversal,
      						 req.body.id_instituicao,
                   req.body.resposta,
      						 req.params.id];
			query = mysql.format(query,table);
			connection.query(query, function(err, rows) {
				if (err) {
					console.log(err);
					res.json({"Error" : true, "Message" : "Erro ao executar a operação 001. Contate o administrador."});
				}
				else {
					var queryDel = "DELETE FROM `opcao` WHERE `id` NOT IN (0";
					for (var i=0;i<req.body.opcoes.length;i++){
						if (req.body.opcoes[i].id != 0) {
							queryDel += ","+req.body.opcoes[i].id;
						}
					}
					queryDel += ") AND id_questao = "+req.params.id+";";
					connection.query(queryDel, function(err, rows) {
							if (err) {
								console.log(err);
								res.json({"Error" : true, "Message" : "Erro ao executar a operação 002. Contate o administrador."});
							}
					});
					for (var i=0;i<req.body.opcoes.length;i++){
						if (req.body.opcoes[i].id != 0) {
							var query = "UPDATE `opcao` SET `nome` = ? WHERE `id` = ?";
							var table = [req.body.opcoes[i].nome, req.body.opcoes[i].id];
						}
						else
						{
							var query = "INSERT INTO `opcao` VALUES (0,?,?)";
							var table = [req.body.opcoes[i].nome, req.params.id];
						}
						query = mysql.format(query,table);
						connection.query(query, function(err, rows) {
							if (err) {
								console.log(err);
								res.json({"Error" : true, "Message" : "Erro ao executar a operação 003. Contate o administrador."});
							}
						});
					}
					res.json({"Error" : false, "Message" : "Questão atualizada com sucesso."});
					connection.release();
				}
			});
		}
	});
}

exports.deleteQuestao = function (req, res) {
	pool.getConnection(function(err, connection) {
		if (err)
			res.json({"Error" : true, "Message" : err});
		else {
			connection.query("DELETE FROM `opcao` WHERE `id_questao` = ?", [req.params.id], function(err, rows) {
				if (err) {
					console.log(err);
					res.json({"Error" : true, "Message" : "Erro ao executar a operação. Contate o administrador."});
				}
				else {
					connection.query("DELETE FROM `questao` WHERE id = ?", [req.params.id], function(err, rows) {
						if (err) {
								console.log(err);
								res.json({"Error" : true, "Message" : "Erro ao executar a operação. Contate o administrador."});
						}
						else res.json({"Error" : false, "Message" : "Questão removida com sucesso."});
					});
				}
			});
			connection.release();
		}
	});
}

exports.aprovarQuestao = function(req, res){
	avaliarQuestao(req.params.id, 2, res);
}

exports.reprovarQuestao = function(req, res){
	avaliarQuestao(req.params.id, 3, res);
}
