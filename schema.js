
exports.instituicao = {
  "id": "/SimpleInstituicao",
  "type": "object",
  "title": "Instituição",
  "properties": {
    "nome": {"title": "Nome", "type": "string", "minLength":1},
    "sigla": {"title": "Sigla", "type": "string", "minLength":1}
  },
  "required": ["nome", "sigla"]
};

//Para operações POST e PUT, o ID poderá ser 0 para opcoes não-existentes no BD
exports.opcao = {
  "id": "/SimpleOpcao",
  "type": "object",
  "title": "Opção",
  "properties": {
	   "id": {"type": "integer", "minLength":1},
     "nome": {"title": "Nome da opção", "type": "string", "minLength":1}
  },
  required: ["id", "nome"]
}

exports.questao = {
  "id": "/SimpleQuestao",
  "type": "object",
  "title": "Questão",
  "properties": {
    "id_instituicao": {"title": "Instituição", "type": "integer", "minLength":1},
    "ano": {"title": "Ano", "type": "integer", "minLength":1},
    "enunciado": {"title": "Enunciado", "type": "string", "minLength":1},
    "opcoes": {
      "title": "Opções",
      "type": "array",
      "minItems": 1,
      "maxItems": 4,
      "items": {"$ref":"/SimpleOpcao"},
      "uniqueItems": true
    },
    "resposta": {"title": "Resposta", "type": "string", "minLength":1},
    "transversal": {"title": "Transversal", "type": "integer"}
  },
  "required": ["enunciado", "ano", "transversal", "resposta", "id_instituicao"]
}
