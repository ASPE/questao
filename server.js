var express = require('express'),
    bodyParser = require('body-parser');
var dbInstituicao = require('./db/instituicao');
var dbQuestao = require('./db/questao');
var dbUsuario = require('./db/usuario');
var app = express();
var middle = function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
  res.setHeader("Content-Type", "application/json");
  next();
};

//Pasta pública com arquivos estáticos
app.use( express.static(process.cwd() + '/public'));
app.use( bodyParser.json());
app.use( bodyParser.urlencoded());
app.use(middle);

//Rotas
app.get('/status/:id', dbQuestao.findStatus);
app.get('/questao/:id/opcoes', dbQuestao.findOpcoes);
app.get('/instituicao', dbInstituicao.findAllInstituicao);
app.get('/instituicao/:id', dbInstituicao.findInstituicao);
app.post('/instituicao', dbInstituicao.newInstituicao);
app.put('/instituicao/:id', dbInstituicao.updateInstituicao);
app.delete('/instituicao/:id', dbInstituicao.deleteInstituicao);
app.get('/questao', dbQuestao.findAllQuestao);
app.get('/questao/:id', dbQuestao.findQuestao);
app.post('/questao', dbQuestao.newQuestao);
app.put('/questao/:id', dbQuestao.updateQuestao);
app.get('/questao/status/:id', dbQuestao.findQuestoesPorStatus);
app.delete('/questao/:id', dbQuestao.deleteQuestao);
app.post('/questao/aprovar/:id', dbQuestao.aprovarQuestao);
app.post('/questao/reprovar/:id', dbQuestao.reprovarQuestao);
app.post('/usuario', dbUsuario.loginUsuario);

app.listen(3001);
console.log('Servidor na porta 3001...');
